'use strict';
const mongoose = require('mongoose');

module.exports.status = async event => {
  return {
    statusCode: 200, 
    body: JSON.stringify({
      message: 'OK'
    })
  }
}
module.exports.createaddress = async event => {
   //creates address
};
module.exports.deleteaddress = async event => {
  //deletes address
};
module.exports.updateaddress = async event => {
  //updates address
};
module.exports.getaddressP = async event => {
  mongoose.connect('mongodb+srv://test:test123@test-s8kcw.gcp.mongodb.net/test?retryWrites=true&w=majority');

  const AddressSchema = mongoose.Schema({
    city: {
      type: String
    },
    state: {
      type: String
    },
    zip: {
      type: String
    }
  })
  const Address = new mongoose.model('Address', AddressSchema);
  const tmp = event.split(':');
  const field = tmp[0], data = tmp[1].replace('}', '');
  const addressData = await Address.find({ [field]: data}).select('address city state zip');
  if (!addressData) {
    return { 
      statusCode: 204,
      body: JSON.stringify({
        message: 'No data for the filter'
      })
    }
  }
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        data: addressData,
      },
      null,
      2
    ),
  };
};
module.exports.getaddress = async event => {
  mongoose.connect('mongodb+srv://test:test123@test-s8kcw.gcp.mongodb.net/test?retryWrites=true&w=majority');

  const AddressSchema = mongoose.Schema({
    city: {
      type: String
    },
    state: {
      type: String
    },
    zip: {
      type: String
    }
  })
  const Address = new mongoose.model('Address', AddressSchema);
  const addressData = await Address.find().limit(2).select('address city state zip');

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        data: addressData,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
